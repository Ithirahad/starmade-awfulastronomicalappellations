package BetterSystemNames;

import BetterSystemNames.Commands.ResetSystemNameCommand;
import BetterSystemNames.Commands.SetSystemNameCommand;
import BetterSystemNames.NameConstruction.DefaultNameParts;
import BetterSystemNames.NameConstruction.NameGen;
import BetterSystemNames.NameConstruction.NamingTemplate;
import api.common.GameClient;
import api.common.GameCommon;
import api.listener.EventPriority;
import api.listener.Listener;
import api.listener.events.controller.ServerInitializeEvent;
import api.listener.events.gui.BigMessagePopupEvent;
import api.listener.events.network.ClientLoginEvent;
import api.listener.events.world.SystemNameGenerationEvent;
import api.listener.events.world.SystemNameGetEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.mod.config.FileConfiguration;
import api.mod.config.SyncedConfigReceiveEvent;
import api.mod.config.SyncedConfigUtil;
import org.newdawn.slick.Color;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.ServerConfig;

import java.util.Random;

import static api.mod.ModStarter.lastConnectedToServer;

//Praise be to the Machine God!
//01100001 01101100 01101100 00100000 01101000 01100001 01101001 01101100 00101110 00101110 00101110 00100000 01100001 01101100 01101100 00100000 01101000 01100001 01101001 01101100 00101110 00101110 00101110 00100000 01100001 01101100 01101100 00100000 01101000 01100001 01101001 01101100 00101110 00101110 00101110

public class Main extends StarMod {
    public static Main modInstance;
    private String[] templateIDs = {"Greco-Roman","Retro Sci-Fi"/*,"Star Catalog"*/}; //TODO: Add custom nameset creation and loading
    public static FileConfiguration nameList;
    public static boolean debug = false;
    private static String currentIP;

    public Main(){modInstance = this;}

    public static void main(String[] args){
        //just to make the env happy lol
    }

    /*
    @Override
    public void onGameStart() {
        setModName("AwfulAstronomicalAppellations");
        setModAuthor("Ithirahad");
        setModVersion("1.21 - Protostar");
        setModDescription("AAA: Deeper-level name generation for better diversity. Also custom admin naming!");
    }
     */
    public void pointNameList(){
        nameList = getConfig(currentNameList());
        if(debug) System.err.println("[MOD][AAA] Pointed to name list as having name " + currentNameList());
    };

    @Override
    public void onServerCreated(ServerInitializeEvent event) {
        super.onServerCreated(event);
        pointNameList();
    }

    @Override
    public void onEnable() {
        DefaultNameParts.populate();

        StarLoader.registerListener(ClientLoginEvent.class, new Listener<ClientLoginEvent>() {
            @Override
            public void onEvent(ClientLoginEvent event) {
                if(GameCommon.isDedicatedServer()){
                    if(debug) {
                        System.err.println("[MOD][AAA] Found " + Main.nameList.getKeys().size() + " custom system name entries.");
                        System.err.println("[MOD][AAA] Sending customized names to client");
                    }
                    pointNameList();
                    SyncedConfigUtil.sendConfigToClient(event.getServerProcessor(), nameList);
                }
                else {
                    if(debug) System.err.println("[MOD][AAA] Not dedicated server; skipping namelist update.");
                }
            }
        }, this);

        StarLoader.registerListener(SyncedConfigReceiveEvent.class, new Listener<SyncedConfigReceiveEvent>() {
            @Override
            public void onEvent(SyncedConfigReceiveEvent event) {
                nameList = event.getConfig();
                nameList.saveConfig(); //*shrug*
            }
        }, this);

        StarLoader.registerListener(SystemNameGetEvent.class, new Listener<SystemNameGetEvent>(EventPriority.LOWEST) {//This is to be treated as the name of the system. Anything else (e.g. faction configurable naming) must overwrite this.
            @Override
            public void onEvent(SystemNameGetEvent s) {
                Vector3i pos = s.getPosition();
                pos.add(-64,-64,-64);
                String centerOriginPos = pos.toString();

                String name = nameList.getString(centerOriginPos);
                if(name != null){
                    s.setName(name);
                }
            }
        }, this);
        StarLoader.registerListener(SystemNameGenerationEvent.class,
                new Listener<SystemNameGenerationEvent>(EventPriority.LOWEST) {//lowest prio because ideally any other mod should be able to override this action
                    @Override
                    public void onEvent(SystemNameGenerationEvent g){
                        NamingTemplate[] templates = {DefaultNameParts.grecoRoman,DefaultNameParts.cheesyScifi/*,DefaultNameParts.starCatalog*/}; //TODO: Make this use template IDs
                        Vector3i sys = g.getSystemCoordinates();
                        Random random = new Random((long) (g.getContainingGalaxy().getSeed()/(sys.hashCode() + 1)));

                        String name = g.getIsVoid()? "Deep Space" : NameGen.build(templates[random.nextInt(templates.length)], g.getContainingGalaxy().getSystemType(g.getSystemCoordinates()), random);

                        g.setName(name);
                        if(debug) System.err.println("Attempting to name system as: " + name);
                    }
                }, this);

        StarLoader.registerListener(BigMessagePopupEvent.class, new Listener<BigMessagePopupEvent>(EventPriority.LOWEST) {
            @Override
            public void onEvent(BigMessagePopupEvent b) {
                GameClientState state = GameClient.getClientState();
                if(b.getUid().contains("SystemChanged")){
                    b.setColor(Color.yellow);

                    Vector3i magicalMysteryMeat = new Vector3i();
                    // I'm just copying the mysterious empty static initialization from the map tools panel lol.
                    // Nothing visible ever gets done with this Vector3i in that implementation.
                    Vector3i loc = new Vector3i(Galaxy.getLocalCoordinatesFromSystem(state.getCurrentClientSystem().getPos(), magicalMysteryMeat));
                    // Honestly no idea what this incantation does and why it should yield results that the other method I tried doesn't, but if it works,
                    // then I'm not going to look a gift-horse in the mouth. Even if it means carrying out arcane rites or sacrificing the occasional goat to the source.

                    if(debug) System.err.println("[MOD][AAA][UI] Position returned as: " + loc.x + ", " + loc.y + ", " + loc.z);
                    String systemName = state.getCurrentGalaxy().getName(loc);
                    b.setTitle(b.getTitle() + " - " + systemName);
                }
            }
        }, this);

        StarLoader.registerCommand(new SetSystemNameCommand());
        StarLoader.registerCommand(new ResetSystemNameCommand());
    }

    public String[] getTemplateIDs() {
        return templateIDs;
    }

    public void setTemplateIDs(String[] templateIDs) {
        this.templateIDs = templateIDs;
    }

    public String currentNameList(){
        String identifier = GameCommon.isClientConnectedToServer() ? GameCommon.getUniqueContextId() : (String)ServerConfig.WORLD.getCurrentState();
        return identifier + "-AssignedNames";
    }

    public static void setSystemName(Vector3i system, String name){
        nameList.set(system.toString(), name);
        nameList.saveConfig();
        if(lastConnectedToServer) SyncedConfigUtil.sendConfigToClients(nameList);
    }
}
