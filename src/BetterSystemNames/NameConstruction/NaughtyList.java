package BetterSystemNames.NameConstruction;

import java.util.ArrayList;
import java.util.Arrays;

public class NaughtyList {
    private static final String[] base = {
            "kid", //making the executive decision to disallow this, not a bad word but not something that should come up
            "retard",
            "idiot",
            "shit",
            "piss",
            "asshol",
            "penis",
            "dick",
            "vagin",
            "cooch",
            "pussy",
            "cunt",
            "wank",
            "shag",
            "fuck",
            "fuk",
            "rape",
            "yiff", //NO!
            "titt",
            "tits",
            "boob",
            "bewb", //lel
            "nigga",
            "nigge", //Originally had "nigg" here but "Shub-Niggurath" exists lol. Granted, that name may have been inspired by racism - the guy's cat was named "Niggerman" for the gods' sakes! - but Lovecraft's influence sci-fi is hard to deny
            "faggo",
            "nazi", //just no.
            "kkk"
    }; //can't include ass/arse (among others) as those are part of plenty of legitimate potential names
    //also that's what additional is for

    private static final String[] religious = {
            "yahweh",
            "hashem",
            "ha'shem",
            "torah",
            "talmud",
            "jew",
            "jesus",
            "bible",
            "christ",
            "allah",
            "quran",
            "qur'an",
            "muslim",
            "islam",
            "hindu",
            "brahma",
            "vishnu",
            "shiva",
            "shinto"
    }; //TODO: implement

    public static ArrayList<String> additional = new ArrayList<>();
    public static String[] everythingBad(){
        ArrayList<String> result = new ArrayList<>();
        result.addAll(Arrays.asList(base));
        result.addAll(additional);
        return result.toArray(new String[additional.size()+ base.length]);
    }

    public static boolean bad(String s){
        boolean isBad = false;
        String lower = s.toLowerCase();
        for(String b : base){
            if(lower.contains(b)){
                isBad = true;
                break;
            }
        }

        if(!isBad) for(String b : additional){
            if(lower.contains(b)){
                isBad = true;
                break;
            }
        }
        return isBad;
    }
}
