package BetterSystemNames.NameConstruction;

import java.io.Serializable;
import java.lang.reflect.Field; //*sigh*
import java.util.LinkedHashMap;

/**
 * Template for procedural naming of stellar systems.
 * TODO: Generation from config
 */
public class NamingTemplate implements Serializable, Cloneable {
    public final String ID;
    /*
     * All LinkedHashMaps of strings are by weight per mil. >=1000 is always a candidate, 500 is half as common, etc.
     * The name builder will create a method to compose an array of possibilities from a LinkedHashMap, adding each value in the arrays (key/100) of the time.
     */
    public LinkedHashMap<Integer,String[]> preAppendsGeneric = new LinkedHashMap<>(); //For titles or other things. e.g. New, Alpha/Beta/etc
    public LinkedHashMap<Integer,String[]> postAppendsGeneric = new LinkedHashMap<>(); //Same as above, but placed after the end of the name.

    public LinkedHashMap<Integer,String[]> preAppendsStar = new LinkedHashMap<>();
    public LinkedHashMap<Integer,String[]> postAppendsStar = new LinkedHashMap<>();

    public LinkedHashMap<Integer,String[]> preAppendsGiant = new LinkedHashMap<>();
    public LinkedHashMap<Integer,String[]> postAppendsGiant = new LinkedHashMap<>();

    public LinkedHashMap<Integer,String[]> preAppendsBlackhole = new LinkedHashMap<>();
    public LinkedHashMap<Integer,String[]> postAppendsBlackhole = new LinkedHashMap<>();

    public LinkedHashMap<Integer,String[]> preAppendsDoublestar = new LinkedHashMap<>();
    public LinkedHashMap<Integer,String[]> postAppendsDoublestar = new LinkedHashMap<>();

    public LinkedHashMap<Integer,String[]> prefixesGeneric = new LinkedHashMap<>(); //Prefixes usable before any letter
    public LinkedHashMap<Integer,String[]> prefixesVowel = new LinkedHashMap<>(); //Prefixes usable before vowels only
    public LinkedHashMap<Integer,String[]> prefixesIntermediate = new LinkedHashMap<>(); //Prefixes usable before intermediate sounds only
    public LinkedHashMap<Integer,String[]> prefixesConsonant = new LinkedHashMap<>(); //Prefixes usable before consonants only
    public LinkedHashMap<Integer,String[]> prefixesExtras = new LinkedHashMap<>(); //Prefixes usable before extraParts only
    public Integer prefixChance = 1000;

    public LinkedHashMap<Integer,String[]> suffixesGeneric = new LinkedHashMap<>(); //Suffixes usable after any letter
    public LinkedHashMap<Integer,String[]> suffixesVowel = new LinkedHashMap<>(); //Suffixes usable after vowels only
    public LinkedHashMap<Integer,String[]> suffixesIntermediate = new LinkedHashMap<>(); //suffixes usable before intermediate sounds only
    public LinkedHashMap<Integer,String[]> suffixesConsonant = new LinkedHashMap<>(); //Suffixes usable after consonants only
    public LinkedHashMap<Integer,String[]> suffixesExtras = new LinkedHashMap<>(); //Suffixes usable after extraParts only
    public Integer suffixChance = 1000;

    public LinkedHashMap<Integer,String[]> fixedSyllables = new LinkedHashMap<>();
    public Integer fixedSyllableWeight = 0; //how often (out of 1000) to use fixed syllables vs. generated ones

    public LinkedHashMap<Integer,String[]> consonants = new LinkedHashMap<>();
    public LinkedHashMap<Integer,String[]> intermediates = new LinkedHashMap<>(); //sonorants and semivowels like l, r, m, n, w, y, etc.
    public LinkedHashMap<Integer,String[]> vowels = new LinkedHashMap<>();
    public LinkedHashMap<Integer,String[]> extraParts = new LinkedHashMap<>(); //anything else you might want to throw into a name, e.g. apostrophes or dashes.
    // Uses optional choose algorithm

    public LinkedHashMap<Integer,String[]> allowedSyllables = new LinkedHashMap<>();
    // allowed syllable structures of consonants (C), intermediates (I), vowels (V), and extra parts (X).
    // Backslash is an escape character, allowing reserved chars to fallthrough. e.g. \X will turn into a capital X in the generated name rather than an extra part.
    // If you want an actual backslash in the syllable structure, use "\\".
    // Be aware that non reserved characters at the start or end of a name's structure disable selection of non generic prefixes/suffixes.

    public Integer allowedClusterLength = 3; // how many consonants can be chained. Further consonants will simply be culled from the syllable structure.
    public Integer insertExtrasWeight = 0; //Percent of syllable gaps that will have extras inserted

    public Integer minSyllableCount = 1;
    public Integer maxSyllableCount = 3; //Bounds on the amount of core syllables per word (not including prefixes, suffixes, or appends)

    public Integer fixedSyllableCount = 2;
    public Integer fixedSyllableCountChance = 500;

    public Integer minLength = 3; //Minimum length (in characters) of name, else regenerate
    public Integer maxLength = 100; //Maximum length of name, else regenerate

    public String[] forbiddenSubstrings = {}; // substrings that are not allowed. Will force regeneration if detected.
    public String[] forbiddenSyllablePatternInitial = {}; // substring not allowed at the beginning of a syllable. Will regenerate the syllable if detected
    public String[] forbiddenSyllablePatternFinal = {}; // substring not allowed at the end of a syllable. Will regenerate the syllable if detected
    public String[] forbiddenSyllables = {}; //syllables not allowed by this template.
    public LinkedHashMap<String,String[]> replace = new LinkedHashMap<>(); //substitute the key with one of the strings in the value array. Supports (or should support?) regex.
    public String[] structureBlacklist = {}; // syllable-notation sequences that are not allowed (e.g. VVV triple vowels). Will force regeneration if detected.

    public Boolean useDefaultAppends = true; //Whether or not to use the default appends found in defaultNameParts.
    public int defaultAppendChance = 100; //weight of all default appends
    public Boolean allowBothAppends = false; //Whether or not to allow both pre and post appends.

    public NamingTemplate(String ID) {
        this.ID = ID;
    }

    @Override
    public NamingTemplate clone(){
        try {
            NamingTemplate t = (NamingTemplate)super.clone();
            Field s = getClass().getDeclaredField("ID");

            s.setAccessible(true);
            s.set(t, ("Clone of " + ID));

            /*This is physically painful, but clone's janky to begin with,
             and I see no other way to copy this many fields without dying inside in the process.
             The non-internal-death-inducing alternative, of course, would be to just kinda...
             leave the ID as duplicate of the original...
             Which is really not what I want out of ID and makes debugging confusing when it's the only
             meaningful identifier I've got for these damnable objects.*/

            s.setAccessible(false);

            return t;
        }
        catch(Exception ignore){
            return new NamingTemplate("FAILURE TO COPY"); //This is terrible error handling and I feel terrible.

            /*Then again this whole thing is terrible. Thankfully it's just a StarMade mod. FML.
            Honestly the responsible thing to do would be to just restructure everything so that templates
            have internal methods to generate substrings and we don't have to fiddle with cloning
            but again: StarMade mod
            When in Rome... :DDDDDDDDDD*/

        }
    }

    //TODO: star pre/post appends

    /*
    public static NamingTemplate fromConfig(SomeKindOfTextSource source){
        //TODO
    }
     */
}
