package BetterSystemNames.NameConstruction;


import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;


public class DefaultNameParts {
    public static String[] greekLetters = {"alpha","beta","gamma","delta","epsilon","zeta","eta","theta","iota","kappa","lambda","mu","nu","xi","omicron","pi","rho","sigma","tau","phi","chi","psi","omega"};

    public static String[] defaultGenericPreAppends = {};
    public static String[] defaultGenericPostAppends = {};
    public static String[] defaultStarPreAppends = {};
    public static String[] defaultStarPostAppends = {"'s Star"};
    public static String[] defaultGiantPreAppends = {};
    public static String[] defaultGiantPostAppends = {};
    public static String[] defaultBlackholePreAppends = {"Great ","Eye of "};
    public static String[] defaultBlackholePostAppends = {" Maw"," Vortex", " Maelstrom", " Whirlpool", " Black Hole"};
    public static String[] defaultDoublestarPreAppends = {"I/II "};
    public static String[] defaultDoublestarPostAppends = {"'s Stars", " A & B"};
    //TYPE_SUN = 0; TYPE_GIANT = 1; TYPE_BLACK_HOLE = 2; TYPE_DOUBLE_STAR = 3.

    public static NamingTemplate grecoRoman = new NamingTemplate("Greco-Roman");
    public static NamingTemplate cheesyScifi = new NamingTemplate("Cheesy Sci-Fi");
    // public static NamingTemplate starCatalog = new NamingTemplate("Star Catalog"); //TODO

    public static LinkedHashMap<Integer,String[]> defaultAppends(int starType, int chance, boolean pre){
        String[] s;
        if(starType == 0){
            s = pre ? defaultStarPreAppends : defaultStarPostAppends;
        }
        else if(starType == 1){
            s = pre ? defaultGiantPreAppends : defaultGiantPostAppends;
        }
        else if(starType == 2){
            s = pre ? defaultBlackholePreAppends : defaultBlackholePostAppends;
        }
        else if(starType == 3){
            s = pre ? defaultDoublestarPreAppends : defaultDoublestarPostAppends;
        }
        else{
            s = pre ? defaultGenericPreAppends : defaultGenericPostAppends;
        }

        String[] g = pre ? defaultGenericPreAppends : defaultGenericPostAppends;

        s = ArrayUtils.addAll(g,s);

        LinkedHashMap<Integer,String[]> result = new LinkedHashMap<>();
        result.put(chance,s);
        return result;
    }

    public static void populate(){
        //Greco-Roman
        grecoRoman.suffixesGeneric.put(800, new String[]{"a","ia", "aea", "os", "us", "is", "ius", "ae", "es", "um","er"});
        grecoRoman.suffixesGeneric.put(350,new String[]{"ax","ix","ices","ad","ides","ites","arch","etis"});
        grecoRoman.suffixesGeneric.put(50,new String[]{"archus","ikos","ra"});
        grecoRoman.suffixesVowel.put(250,new String[]{"tis","d","ad","dum","no","x","od","bus","nos"});
        grecoRoman.suffixesConsonant.put(250,new String[]{"inia","ibus","ion"});
        grecoRoman.suffixesIntermediate.put(800, new String[]{"on","ion"});

        grecoRoman.suffixChance = 1000;
        grecoRoman.prefixChance = 0;

        grecoRoman.intermediates.put(1000,new String[]{"l","m","n","r","s"}); //S needs to be there to form constructions like "Erasmus" or "Aestus" without also forming much worse constructions

        grecoRoman.consonants.put(1000,new String[]{"c","c","d","f","l","m","n","p","qu","r","s","t","v","y","ph","th","gn"});
        grecoRoman.consonants.put(650,new String[]{"b","g","h","j","k","x","ch"});
        grecoRoman.consonants.put(200, new String[]{"z","k","kh"});

        grecoRoman.vowels.put(1000,new String[]{"a","ae","e","i","io","o","u"});
        grecoRoman.vowels.put(200,new String[]{"y","eu","ai"});

        grecoRoman.allowedSyllables.put(1000,new String[]{"CV"});
        grecoRoman.allowedSyllables.put(400, new String[]{"CVI"});
        grecoRoman.allowedSyllables.put(300,new String[]{"CVCI"});
        grecoRoman.allowedSyllables.put(250,new String[]{"CIV","CIVI","VI"});

        grecoRoman.forbiddenSyllablePatternFinal = new String[]{"CVCI"};

        grecoRoman.fixedSyllables.put(20,new String[]{"icci"});

        grecoRoman.replace.put("om\\b",new String[]{"um","on"});
        grecoRoman.replace.put("aa",new String[]{"a"});
        grecoRoman.replace.put("mx",new String[]{"m","nx"});
        grecoRoman.replace.put("sx",new String[]{"x"});
        grecoRoman.replace.put("ao",new String[]{"a","o"});
        grecoRoman.replace.put("[^aeiou\\s]gn",new String[]{"gn"}); //Strip consonant before gn
        grecoRoman.replace.put("gn[^aeiou\\s]",new String[]{"gn"}); //Strip consonant after gn
        grecoRoman.replace.put("gn\\b",new String[]{"n"}); //strip gn at end

        grecoRoman.minSyllableCount = 1;
        grecoRoman.maxSyllableCount = 3;
        grecoRoman.fixedSyllableCount = 2;
        grecoRoman.fixedSyllableCountChance = 700;
        grecoRoman.minLength = 4;
        grecoRoman.maxLength = 16;

        grecoRoman.forbiddenSubstrings = new String[]{"kid"}; //Should be pretty rare with this generator anyway, but some of the rarer appends are problematic with this

        grecoRoman.useDefaultAppends = true;
        grecoRoman.defaultAppendChance = 50;

        //------------------------------------------------------------

        cheesyScifi.prefixesGeneric.put(20, new String[]{"xe","min"}); //Thanks to Minbar, the Minmatar, and Mintaka, this "Min-" prefix had to be included.
        cheesyScifi.prefixChance = 25;

        cheesyScifi.suffixesGeneric.put(800, new String[]{"ix","ox","ti","id","oid","ian","imus","og","etra"});
        cheesyScifi.suffixesGeneric.put(700, new String[]{"a", "ex", "ia", "ae", "aea", "os", "us", "is", "ius", "ae", "es", "on", "um","er","ces","don","gon","orn", "gor", "bor", "vor"});
        cheesyScifi.suffixesGeneric.put(400, new String[]{"mog","cron","enon","von","vog","bion","tus","orius","akk","drakk","drax","grax","lax","nax","lus"}); //extra layers of cheese
        cheesyScifi.suffixesConsonant.put(350,new String[]{"inia","ibus","nia","bus","e","en"});
        cheesyScifi.suffixesGeneric.put(100, new String[]{"ax","ies"});
        cheesyScifi.suffixesGeneric.put(40, new String[]{"frey","lore","pocalypse","-dum","tron","burn","bolg","bannog","gard","heim","phagor"}); //goofy easter eggs
        cheesyScifi.suffixChance = 500;

        ArrayList<String> glp = new ArrayList<>();
        for(String s : greekLetters){
            glp.add(s.substring(0, 1).toUpperCase() + s.substring(1) + " ");
            //Add capitalized greek letter name with space
        }

        String[] grs = glp.toArray(new String[glp.size()]);

        cheesyScifi.preAppendsGeneric.put(100,grs);
        cheesyScifi.preAppendsGeneric.put(100, new String[]{"Ultima ","Proxima ","Nova ","Termina ","Prima "});
        cheesyScifi.preAppendsStar.put(80, new String[]{"AA ","AB ","AL ","BR ","CC ", "CG ", "D ", "EC ", "EE ", "EF ", "EL ", "F ", "FZ ", "GN ", "H ", "HL ", "HH ", "HZ ", "IX ", "JC ", "JY ", "LL ", "LN ", "LR ", "MN ", "OE ", "OU ", "PK ", "Q ", "QV ", "QX ", "QZ ", "RR ", "RS ", "RV ", "SN ", "SV ", "T ", "TL ", "TZ ", "UU ", "VB ", "VD ", "VQ ", "VV ", "X ", "XA ", "XB ", "XF ", "XJ ", "XZ ", "Y ", "YH ", "Z ", "ZB ", "ZD ", "ZM "});
        cheesyScifi.preAppendsDoublestar.put(80, new String[]{"AA ","AB ","AL ","BR ","CC ", "CG ", "D ", "EC ", "EE ", "EF ", "EL ", "F ", "FZ ", "GN ", "H ", "HL ", "HH ", "HZ ", "IX ", "JC ", "JY ", "LL ", "LN ", "LR ", "MN ", "OE ", "OU ", "PK ", "Q ", "QV ", "QX ", "QZ ", "RR ", "RS ", "RV ", "SN ", "SV ", "T ", "TL ", "TZ ", "UU ", "VB ", "VD ", "VQ ", "VV ", "X ", "XA ", "XB ", "XF ", "XJ ", "XZ ", "Y ", "YH ", "Z ", "ZB ", "ZD ", "ZM "});
        cheesyScifi.preAppendsGeneric.put(25, new String[]{"Accursed ","Forgotten ","Ancient ","Fallen ","Sleeping ","Weeping ","Burning ","Unending ","Forlorn ","Waking ","Unspoken "});
        cheesyScifi.preAppendsDoublestar.put(500, new String[]{"A/B ","P/Q ", "R/S ", "X/Y "});
        cheesyScifi.preAppendsBlackhole.put(50,new String[]{"Whirling ", "Prismatic "});
        cheesyScifi.preAppendsBlackhole.put(10,new String[]{"Mad ","Screaming ","Hungering ","Thirsty ","Unknown ","Gate of ","Broken ","Darkest "});

        cheesyScifi.postAppendsStar.put(70, new String[]{" Major", " Minor", " Majoris", " Minoris"});
        cheesyScifi.postAppendsGeneric.put(200,new String[]{" Core", " Prime", " Primae", " Majoris", " Major"});
        cheesyScifi.postAppendsBlackhole.put(500,new String[]{" Wormhole", " Vortex"});
        cheesyScifi.postAppendsBlackhole.put(500,new String[]{" Anomaly", " Bridge", " Passage", " Passageway"});
        cheesyScifi.postAppendsBlackhole.put(20,new String[]{"'s Bridge", "Causeway", "'s Causeway"," Portal", "'s Portal", " Eye", "'s Eye", "'s Passage", "'s Maelstrom"});

        cheesyScifi.defaultAppendChance = 70;
        cheesyScifi.useDefaultAppends = false;
        cheesyScifi.allowBothAppends = false;

        cheesyScifi.intermediates.put(1000,new String[]{"l","m","n","r"});
        cheesyScifi.intermediates.put(400, new String[]{"s","ng"});
        cheesyScifi.intermediates.put(200, new String[]{"w","y"});
        cheesyScifi.intermediates.put(40, new String[]{"j","x","zh"});

        cheesyScifi.extraParts.put(1000, new String[]{"-"});
        cheesyScifi.insertExtrasWeight = 25; //1 in 20

        cheesyScifi.consonants.put(1000,new String[]{"b","c","d","f","j","k","l","m","n","p","qu","r","s","t","u","v","x","y","z","ph","th","kh"});
        cheesyScifi.consonants.put(300,new String[]{"b","g","h","ng"});
        cheesyScifi.consonants.put(200, new String[]{"k","q","rr","w"});
        cheesyScifi.consonants.put(60, new String[]{"ts","gg","zh"});

        cheesyScifi.vowels.put(1000,new String[]{"a","e","i","i","o","u"});
        cheesyScifi.vowels.put(250,new String[]{"ae","ou","mu","nu"});
        cheesyScifi.vowels.put(100,new String[]{"y","ie","ei","eu"});
        cheesyScifi.vowels.put(50,new String[]{"ee","oo"}); //extra silly double vowels

        //Gallifrey - CVI IV CIVI (or CVI IV $suffix)
        //Trenzalore - CIVI CV $suffix
        //Andoria - VS CVI $suffix
        //Kronos - CIVI $suffix
        //Cybertron - CI CVI $suffix
        //Minbar - CVI CVI
        //Aier - VVI
        //TODO: Do something about long prefixes + long syllables...

        cheesyScifi.allowedSyllables.put(1000,new String[]{"V","CV","CVI"});
        cheesyScifi.allowedSyllables.put(70, new String[]{"VC","IV","VI"}); //rare-ish
        cheesyScifi.allowedSyllables.put(300,new String[]{"CIVI","CIV"});

        cheesyScifi.fixedSyllables.put(400,greekLetters);
        cheesyScifi.fixedSyllableWeight = 35; //rare

        cheesyScifi.replace.put("uuu",new String[]{"uu","u"});
        cheesyScifi.replace.put("eeoo",new String[]{"oo","ee","eloo","eelo"});
        cheesyScifi.replace.put("[^aeiou\\s]gn",new String[]{"gn"}); //Strip consonants before gn
        cheesyScifi.replace.put("gn[^aeiou\\s]",new String[]{"gn"}); //Strip consonants after gn
        cheesyScifi.replace.put("gn\\b",new String[]{"g"}); //strip gn at end

        cheesyScifi.forbiddenSubstrings = new String[]{"kid"}; //some of the rarer appends are problematic with this

        cheesyScifi.minSyllableCount = 1;
        cheesyScifi.maxSyllableCount = 3;

        cheesyScifi.minLength = 3; //Sol
        cheesyScifi.maxLength = 24; //Raxacoricofallipatorius!

        //------------------------------------------------------------

        /*
        starCatalog.consonants.put(1000, new String[]{"B","C","D","F","G","H","J","K","L","M","N","P","Q","R","S","T","V","W","X","Y","Z"});
        starCatalog.vowels.put(1000, new String[]{"A","E","I","O","U"});
        starCatalog.intermediates.put(1000, new String[]{"1","2","3","4","5","6","7","8","9","0"}); //co-opting for numerals

        starCatalog.preAppendsGeneric.put(200,grs);
        starCatalog.preAppendsDoublestar.put(1000,new String[]{"BIN ","DS "});
        starCatalog.postAppendsDoublestar.put(1000,new String[]{" AB", " A/B", " A & B", " 1/2", " X/Y", " Alpha & Beta", " Major & Minor"});
        starCatalog.preAppendsBlackhole.put(1000,new String[]{"BH ","Q-","X-","WH "});

        starCatalog.prefixChance = 1000;
        starCatalog.prefixesGeneric.put(1000, new String[]{"CEL ","ET-","GIO-","SC-","TGC ","UCC ","XC-"});
        starCatalog.allowedSyllables.put(1000, new String[]{"G\\C-V","C-C","C-I","VIC-C","C-I","V-C","CC-C","CC-V"});
        starCatalog.allowedSyllables.put(999, new String[]{"I-I"}); //effectively a 50 percent chance of adding a purely numeric syllable
        //TODO: rewrite using extras
        //starCatalog.extras.put(1000, new String[]{"-"});
        //starCatalog.insertExtrasWeight = 1000;

        starCatalog.suffixChance = 75;
        starCatalog.suffixesGeneric.put(1000, new String[]{"/a","/b","/c","/d","i","q","v","/x","/z","-I","-II","-III","-IV","-V","-VI","-VII","-IX","-X","maj","prm"});

        starCatalog.minSyllableCount = 2;
        starCatalog.maxSyllableCount = 6;

        starCatalog.minLength = 6;
        starCatalog.maxLength = 30; //whatever

        starCatalog.useDefaultAppends = false;
        */
    }
}
