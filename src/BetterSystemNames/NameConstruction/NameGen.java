package BetterSystemNames.NameConstruction;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;

import static BetterSystemNames.NameConstruction.NamingUtils.mergeMapsOfArrays;
import static BetterSystemNames.Main.*;

public class NameGen {
    static int maxAttempts = 200;

    public static String build(NamingTemplate source, Random rng){
        return build(source,0,rng);
    }

    public static String build(NamingTemplate source, int starType, Random random){
        String name = "";
        String debugInfo = "";
        String debugStep = "Choosing Syllable Count";
        int syllableCount;
        int attempts = 0;

        //try to choose syllable count.
        if (source.maxSyllableCount != 0 && !wFlip(source.fixedSyllableWeight, random)) {
            int n = Math.max(1,source.minSyllableCount);
            //Sanity-correct minimum length to 1. (compensate for user derping)
            syllableCount = n + random.nextInt(1 + source.maxSyllableCount - n);
        }
        else syllableCount = source.fixedSyllableCount;

        try {
            //assert syllableCount > 0;
            if(syllableCount <= 0){
                throw new Exception("Syllable count is zero!");
            }
            AttemptingNameGeneration: do {
                name = ""; //Start from scratch every time
                ArrayList<String> syllables = new ArrayList<>(); //our syllable collection

                debugStep = "Choosing whether to use a prefix and/or suffix";
                boolean usePrefix = wFlip(source.prefixChance, random);
                String prefix = "";
                boolean useSuffix = wFlip(source.suffixChance, random);
                String suffix = "";

                debugInfo = "<<" + source.ID;
                if(usePrefix) debugInfo += " WithPre,";
                if(useSuffix) debugInfo += " WithSuff,";

                if (!(source.consonants.isEmpty() && source.vowels.isEmpty() && source.intermediates.isEmpty()) && !source.allowedSyllables.isEmpty()) {
                    debugInfo += " UsingDynSyls (" + syllableCount + ")";
                    //we have letters to draw from, and at least one syllable structure.
                    ArrayList<String> skeletons = new ArrayList<>(); //Spooky scary syllable structure sequence sends shivers down my spine

                    for(int i=1; i <= syllableCount; i++){
                        debugStep = "Building dynamic syllable skeletons (position " + i + ")";
                        //for each syllable slot...
                        String syl;
                        do {
                            if (i == 0) {
                                do {
                                    syl = choose(source.allowedSyllables, random);
                                }
                                while (ArrayUtils.contains(source.forbiddenSyllablePatternInitial, syl));
                            } else if (i == syllableCount) {
                                do {
                                    syl = choose(source.allowedSyllables, random);
                                }
                                while (ArrayUtils.contains(source.forbiddenSyllablePatternFinal, syl));
                            } else syl = choose(source.allowedSyllables, random);
                        } while (ArrayUtils.contains(source.forbiddenSyllables, syl));

                        boolean containsForbidden = false;

                        if(source.structureBlacklist.length != 0) {
                            StringBuilder soFar = new StringBuilder();
                            for (String s : skeletons) {
                                soFar.append(s);
                            }
                            soFar.append(syl);

                            containsForbidden = ArrayUtils.contains(source.structureBlacklist, soFar.toString());

                            if (containsForbidden) i--;
                        }

                        if(!containsForbidden) skeletons.add(syl);
                    }


                    //Now, we have a series of syllable skeletons, like: CV, CVL, CVLX, etc.
                    //So it's time to pick affixes.
                    if(usePrefix) {
                        debugStep = "Choosing prefix";
                        char c = firstChar(skeletons.get(0));
                        LinkedHashMap<Integer,String[]> candidates = new LinkedHashMap<>(source.prefixesGeneric);

                        if(c == 'C') candidates = mergeMapsOfArrays(candidates,source.prefixesConsonant);
                        else if(c == 'I') candidates = mergeMapsOfArrays(candidates,source.prefixesIntermediate);
                        else if(c == 'V') candidates = mergeMapsOfArrays(candidates,source.prefixesVowel);
                        else if(c == 'X') candidates = mergeMapsOfArrays(candidates,source.prefixesExtras);
                        //else just use generic
                        prefix = choose(candidates, random);
                    }
                    if(useSuffix) {
                        debugStep = "Choosing suffix";
                        String f = skeletons.get(skeletons.size() - 1);
                        char c = lastChar(f);
                        LinkedHashMap<Integer,String[]> candidates = new LinkedHashMap<>(source.suffixesGeneric);

                        if(c == 'C') candidates = mergeMapsOfArrays(candidates,source.suffixesConsonant);
                        else if(c == 'I') candidates = mergeMapsOfArrays(candidates,source.suffixesIntermediate);
                        else if(c == 'V') candidates = mergeMapsOfArrays(candidates,source.suffixesVowel);
                        else if(c == 'X') candidates = mergeMapsOfArrays(candidates,source.suffixesExtras);
                        //else just use generic
                        suffix = choose(candidates, random);
                    }

                    //TODO: Anything to be done with the skeletons goes here; after this they are processed.

                    for (String pattern : skeletons){
                        debugStep = "Building procedural syllable from pattern \"" + pattern + "\")";
                        //The final, "actual" syllable
                        boolean useFixed = (!source.fixedSyllables.isEmpty()) && wFlip(source.fixedSyllableWeight, random);
                        String syllable;

                        if(useFixed) syllable = choose(source.fixedSyllables, random);
                        else syllable = generateSyllable(pattern, source, random);

                        //syllable += "."; //TODO: Add separator for debug
                        if(syllable.trim().isEmpty()) throw new Exception("Returned syllable is empty!");
                        syllables.add(syllable);
                    }
                } else if (source.fixedSyllables.size() != 0) {
                    String reason = "Reason: ";
                    if(source.consonants.isEmpty() && source.vowels.isEmpty() && source.intermediates.isEmpty()) reason += "NoLetters";
                    if(source.allowedSyllables.isEmpty()) reason += "NoStructures";
                    debugStep = "Building fixed syllable. " + reason;

                    debugInfo += " UsingStatSyls (" + syllableCount + "; " + reason + ")";
                    //build from fixed syllables only
                    for(int i = 1; i < syllableCount; i++){
                        LinkedHashMap<Integer, String[]> possibilities = new LinkedHashMap<>(source.fixedSyllables);
                        syllables.add(choose(possibilities,random));
                    }
                }

                debugInfo += ">> ";

                //name += "."; //Add separator for debug

                for (int i = 0; i < syllables.size(); i++) {
                    debugStep = "Adding extras (Position " + i + ")";
                    boolean useExtra = wFlip(source.insertExtrasWeight,random);
                    if(0 < i && i < (syllables.size() - 1) && useExtra) name += chooseOptional(source.extraParts,random);
                    //Try appending extras everywhere but the beginning

                    String syllable = syllables.get(i);
                    name += syllable;
                }

                //name += "."; //Add separator for debug
                debugStep = "Checking for empty";
                //assert !(name.replaceAll("\\s+","").isEmpty());
                //assert !(name.trim().isEmpty()); //In case my regex is wrong
                if(name.trim().isEmpty()) throw new Exception("Generated empty nucleus!");
                //At this point, the string should be made of syllables, and those syllables shouldn't be made of nothing.

                debugStep = "Adding affixes";
                if (usePrefix) name = prefix + name;
                if (useSuffix) name += suffix;

                //TODO: Implement replace at end/start feature
                //TODO: Implement multiword

                debugStep = "Capitalizing & Validating Eligibility";
                if(name.length() > 1 && name.length() >= source.minLength) name = name.substring(0, 1).toUpperCase() + name.substring(1);
                else if(source.minLength > 1){
                    attempts++;
                    continue AttemptingNameGeneration;
                }
                else{
                    //Special case: single-letter (or blank) names permitted
                    name = name.toUpperCase();
                }

                String preAppend = "";
                String postAppend = "";
                //Select appends
                LinkedHashMap<Integer, String[]> appends;
                LinkedHashMap<Integer, String[]> unique;
                LinkedHashMap<Integer, String[]> generic;

                boolean appendFront = random.nextBoolean();

                //Add appends
                //TYPE_SUN = 0; TYPE_GIANT = 1; TYPE_BLACK_HOLE = 2; TYPE_DOUBLE_STAR = 3.

                if(appendFront || source.allowBothAppends) {
                    debugStep = "Compiling pre appends";
                    generic = source.preAppendsGeneric; //TODO: Consider moving the type specific pres and posts to a nested map
                    if(debug) System.err.println("[MOD][BetterSystemNames] Reading StarType " + starType);

                    if (starType == 0) {
                        if(debug) System.err.println("[MOD][BetterSystemNames] System type is 0 (Star); choosing appends accordingly.");
                        unique = mergeMapsOfArrays(generic, source.preAppendsStar); //TODO: Debug tracing on these
                        for (String[] strings : unique.values())
                            for (String string : strings)
                                if(debug) System.err.println("Can see unique preappend option: " + string);
                    } else if (starType == 1) {
                        if(debug) System.err.println("[MOD][BetterSystemNames] System type is 1 (Giant); choosing appends accordingly.");
                        unique = mergeMapsOfArrays(generic, source.preAppendsGiant);
                    } else if (starType == 2) {
                        if(debug) System.err.println("[MOD][BetterSystemNames] System type is 2 (Black Hole); choosing appends accordingly.");
                        unique = mergeMapsOfArrays(generic, source.preAppendsBlackhole);
                    } else if (starType == 3) {
                        if(debug) System.err.println("[MOD][BetterSystemNames] System type is 3 (Double Star); choosing appends accordingly.");
                        unique = mergeMapsOfArrays(generic, source.preAppendsDoublestar);
                    } else {
                        if(debug) System.err.println("[MOD][BetterSystemNames] System type is 'other'; defaulting to generic pre append for this naming template.");
                        unique = generic;
                    }

                    //Merge in default appends for this stellar type, or not.
                    if (source.useDefaultAppends) {
                        appends = mergeMapsOfArrays(unique, DefaultNameParts.defaultAppends(starType, source.defaultAppendChance, true));
                    } else {
                        appends = unique;
                    }
                    debugStep = "Selecting pre append out of selection of size " + appends.size();
                    if (!appends.isEmpty())
                        preAppend = chooseOptional(appends,random);

                    appends.clear();
                }

                if(!appendFront || source.allowBothAppends) {
                    debugStep = "Compiling post appends";
                    generic = source.postAppendsGeneric;
                    if (starType == 0 && !source.postAppendsStar.isEmpty()) {
                        if(debug) System.err.println("[MOD][BetterSystemNames] System type is 0 (Star); choosing appends accordingly.");
                        unique = mergeMapsOfArrays(generic, source.postAppendsStar);
                        for (String[] strings : unique.values())
                            for (String string : strings)
                                if(debug) System.err.println("Can see unique postappend option: " + string);
                    } else if (starType == 1) {
                        if(debug) System.err.println("[MOD][BetterSystemNames] System type is 1 (Giant); choosing appends accordingly.");
                        unique = mergeMapsOfArrays(generic, source.postAppendsGiant);
                    } else if (starType == 2) {
                        if(debug) System.err.println("[MOD][BetterSystemNames] System type is 2 (Black Hole); choosing appends accordingly.");
                        unique = mergeMapsOfArrays(generic, source.postAppendsBlackhole);
                    } else if (starType == 3) {
                        if(debug) System.err.println("[MOD][BetterSystemNames] System type is 3 (Double Star); choosing appends accordingly.");
                        unique = mergeMapsOfArrays(generic, source.postAppendsDoublestar);
                    } else {
                        if(debug) System.err.println("[MOD][BetterSystemNames] System type is 'other'; defaulting to generic post append for this naming template.");
                        unique = generic;
                    }

                    //Merge in default appends for this stellar type, or not.
                    if (source.useDefaultAppends) {
                        appends = mergeMapsOfArrays(unique, DefaultNameParts.defaultAppends(starType, source.defaultAppendChance, false));
                    } else {
                        appends = unique;
                    }

                    debugStep = "Selecting post append out of selection of size " + appends.size();
                    if (!appends.isEmpty()) postAppend = chooseOptional(appends,random);
                }

                if(appendFront||source.allowBothAppends) name = preAppend + name;
                if(!appendFront||source.allowBothAppends) name = name + postAppend;

                if (!source.replace.isEmpty()) for (String pattern : source.replace.keySet()) {
                    try {
                        //pattern = "(?i)" + pattern;
                        debugStep = "Running replace rule for pattern \"" + pattern + "\"";
                        String[] replacements = source.replace.get(pattern);
                        if (!pattern.equals("")) {
                            name = Matcher.quoteReplacement(name);
                            name = name.replaceAll(pattern, replacements[random.nextInt(replacements.length)]);
                        }
                    }
                    catch(Exception ex){
                        if(debug) System.err.println("[MOD][BetterSystemNames] Failed to execute replace rule: " + pattern);
                        if(debug) System.err.println(ex.getMessage());
                    }
                }

                name = name.trim();
                //verbose DEBUG: name = debugInfo + name;
                attempts++;
            }
            while ((name.length() < source.minLength || //too small
                    name.length() > source.maxLength || //too big
                    NaughtyList.bad(name.toLowerCase()) || //string contains any naughty substrings
                    StringUtils.indexOfAny(name.toLowerCase(), source.forbiddenSubstrings) != -1 || //string contains anything forbidden by source
                    name.isEmpty() //Can't have an empty name. Shouldn't happen, but bad replace rules could cause it
            ) && attempts < maxAttempts
            );
            if(debug) System.err.println("[MOD][BetterSystemNames] Completed name generation successfully in " + attempts + " attempt(s).");
        }
        catch(Exception ex){
            if(debug) System.err.println("[MOD][BetterSystemNames] Failed to build name!");
            if(debug) System.err.println("Failure at step: " + debugStep);
            if(debug) System.err.println("State of name: \"" + name + "\"");
            if(debug) System.err.println("Info: " + debugInfo);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string
            if(debug) System.err.println(sStackTrace);
            name = "#ERR (" + source.ID + ")";
        }
        if(attempts == maxAttempts){
            name = "#FAIL:" + debugInfo;
        }
        //DEBUG: name = source.ID + " - " + name;
        return name;
    }

    public static String choose(Map<Integer,String[]> source, Random random) throws Exception {
        if(!source.isEmpty()) {
            ArrayList<String> possibilities = new ArrayList<>();
            ArrayList<String> fallback = new ArrayList<>();
            for (Integer key : source.keySet()) {
                if (roll(random) < key) { //Key out of 1000 chance to select one of these
                    String[] strings = source.get(key);
                    if(strings.length > 0) {
                        int index = random.nextInt(strings.length); //Select a random item from the list //TODO: bound must be positive
                        possibilities.add(strings[index]); //Add to possibilities
                    }
                } else {
                    String[] options = source.get(key);
                    int length = options.length;
                    if (length > 0) fallback.add(options[random.nextInt(length)]); //Add one option from this to backup list
                }
            }
            if (possibilities.size() == 0) {
                //We were unlucky, and nothing got chosen at all whatsoever
                //Branch will never be reached if LinkedHashMap contains a key 1000
                if(debug) System.err.println("[MOD][BetterSystemNames] Weight-class-based selection yielded no result. Falling back on pure RNG");
                if (!fallback.isEmpty()) return fallback.get(random.nextInt(fallback.size()));
                else return "";
            } else return possibilities.get(random.nextInt(possibilities.size())); //Random from possibilities
        }
        else throw new Exception("Failed mandatory choose operation from " + source.toString() + "!");
    }

    private static String chooseOptional(LinkedHashMap<Integer,String[]> source,Random random){
        if(!source.isEmpty()) {
            ArrayList<String> possibilities = new ArrayList<>();
            for (Integer key : source.keySet()) {
                if (roll(random) < key) { //Key out of 1000 chance to select one of these
                    String[] strings = source.get(key);
                    if(strings.length > 0) {
                        int index = random.nextInt(strings.length); //Select a random item from the list //TODO: bound must be positive
                        possibilities.add(strings[index]); //Add to possibilities
                    }
                }
            }
            if (!possibilities.isEmpty())
                return possibilities.get(random.nextInt(possibilities.size())); //Random from possibilities
            else return "";
        }
        else return "";
    }

    private static int roll(Random random){
        return random.nextInt(1000) + 1;
    }

    private static boolean wFlip(int weight, Random random){ //Weighted coin flip
        if (weight == 1000) return true;
        else if (weight == 0) return false;
        else return weight >= roll(random);
    }

    private static String generateSyllable(String pattern,NamingTemplate source,Random random) throws Exception{
        StringBuilder result;
        char[] pchars = pattern.toCharArray();
        int attempts = 0;
        String[] badThings = ArrayUtils.addAll(source.forbiddenSyllables,NaughtyList.everythingBad());
        do{
            result = new StringBuilder();
            for(int i=0; i < pchars.length; i++) {
                //For each character in the syllable pattern...
                char C = pchars[i];
                String r;

                if (C == 'C') r = choose(source.consonants,random);
                else if (C == 'I') r = choose(source.intermediates,random);
                else if (C == 'V') r = choose(source.vowels,random);
                else if (C == 'X') r = chooseOptional(source.extraParts,random);
                //substitute the skeleton markup with an actual corresponding sound, chosen by configured weight
                else if (C == '\\') {
                    //ESCAPE CASE
                    i++;
                    r = Character.toString(pchars[i]);
                    //Ignore this character as it's an escape character, look ahead and copy the next character verbatim, and then skip the next one
                }
                else r = Character.toString(C);
                result.append(r); //Add the resulting substring to the full syllable
            }
            if(debug) System.err.println("Attempted to generate character from pattern " + pattern + "; resulting syllable is \"" + result + "\"");
            attempts++;
        } while (attempts < maxAttempts && (ArrayUtils.contains(badThings, result.toString()) || result.length() < pattern.length() /*while a single letter might match multiple, a letter should never match nothing*/));

        if(attempts == maxAttempts)
            if(debug) result = new StringBuilder("[#FAIL]");
            else result = new StringBuilder(""); //Should NEVER happen. TODO: better documentation whenever these happen
        return result.toString();
    }

    public static char lastChar(String s) {
        if (s.equals("") || s == null)
            return ' ';
        return s.charAt(s.length() - 1);
    }

    public static char firstChar(String s) {
        if (s.equals("") || s == null)
            return ' ';
        return s.charAt(0);
    }
}
