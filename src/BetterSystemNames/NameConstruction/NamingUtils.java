package BetterSystemNames.NameConstruction;

import com.google.common.collect.Sets;
import org.apache.commons.lang3.ArrayUtils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

public class NamingUtils {
    static <K,V> LinkedHashMap<K,V[]> mergeMapsOfArrays(LinkedHashMap<K,V[]> a, LinkedHashMap<K,V[]> b){ //TODO: Make this even more generic for later use?
        LinkedHashMap<K,V[]> result = (LinkedHashMap<K,V[]>)a.clone();
        LinkedHashMap<K,V[]> second = (LinkedHashMap<K,V[]>)b.clone();

        Set<K> intersections = Sets.intersection(result.keySet(),second.keySet());
        for(K key : intersections){
            result.put(key, ArrayUtils.addAll(a.get(key),b.get(key)));
            second.remove(key); //so we can safely merge what remains without overwriting what we just did
        }

        result.putAll(second); //finally, do a normal merger of the two since the conflicts are eliminated.
        return result;
    }
}
