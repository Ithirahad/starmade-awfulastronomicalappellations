package BetterSystemNames.Commands;
import api.ModPlayground;
import api.mod.StarMod;
import api.utils.game.chat.CommandInterface;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import javax.annotation.Nullable;

import static BetterSystemNames.Main.modInstance;
import static BetterSystemNames.Main.setSystemName;

public class SetSystemNameCommand implements CommandInterface {
    @Override
    public String getCommand() {
        return "set_system_name";
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getDescription() {
        return "Overrides the default name of a specified star system. If no coordinates are provided, defaults to your current location. Format: /set_system_name MySystemName OR /set_system_name x y z MySystemName";
    }

    @Override
    public boolean isAdminOnly() {
        return true;
    }

    @Override
    public boolean onCommand(PlayerState sender, String[] args) {
        ModPlayground.broadcastMessage("[MOD][AAA] Attempting to rename system...");
        try {
            Vector3i pos;
            String name;

            if(args.length == 4){
                pos = new Vector3i(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
                name = args[3];
            }
            else if(args.length == 1){
                pos = sender.getCurrentSystem();
                name = args[0];
            }
            else throw new IndexOutOfBoundsException();

            setSystemName(pos,name);
            ModPlayground.broadcastMessage("[MOD][AAA] Renamed system " + pos.toString() + " to " + name);
        }
        catch(IndexOutOfBoundsException e) {
            ModPlayground.broadcastMessage("[MOD][AAA] Error: Incorrect number of arguments!");
            ModPlayground.broadcastMessage("[MOD][AAA] Examples: /set_system_name MySystemName OR /set_system_name x y z MySystemName");
        }
        catch(NumberFormatException e){
            ModPlayground.broadcastMessage("[MOD][AAA] Error: Invalid arguments!");
            ModPlayground.broadcastMessage("[MOD][AAA] Examples: /set_system_name MySystemName OR /set_system_name x y z MySystemName");
        }
        catch(Exception e) {
            ModPlayground.broadcastMessage("[MOD][AAA] Error: Error saving name!");
        }
        return true;
    }

    @Override
    public void serverAction(@Nullable PlayerState sender, String[] args) {

    }

    @Override
    public StarMod getMod() {
        return modInstance;
    }
}
