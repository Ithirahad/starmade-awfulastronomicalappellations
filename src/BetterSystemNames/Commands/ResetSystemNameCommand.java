package BetterSystemNames.Commands;

import api.ModPlayground;
import api.mod.StarMod;
import api.utils.game.chat.CommandInterface;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import javax.annotation.Nullable;

import static BetterSystemNames.Main.modInstance;
import static BetterSystemNames.Main.setSystemName;

public class ResetSystemNameCommand implements CommandInterface {
    @Override
    public String getCommand() {
        return "reset_system_name";
    }

    @Override
    public String[] getAliases() {
        return new String[0];
    }

    @Override
    public String getDescription() {
        return "Resets the name of a specified star system. If no coordinates are provided, defaults to your current location.  Format: /reset_system_name OR /reset_system_name x y z";
    }

    @Override
    public boolean isAdminOnly() {
        return true;
    }

    @Override
    public boolean onCommand(PlayerState sender, String[] args) {
        ModPlayground.broadcastMessage("[MOD][AAA] Attempting to reset system name...");
        try {
            Vector3i pos;

            if(args.length == 3) pos = new Vector3i(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
            else if(args.length == 0) pos = sender.getCurrentSystem();
            else throw new IndexOutOfBoundsException();

            setSystemName(pos,null);
            ModPlayground.broadcastMessage("[MOD][AAA] Resetting name of system " + pos.toString() + ".");
        }
        catch(IndexOutOfBoundsException e) {
            ModPlayground.broadcastMessage("[MOD][AAA] Error: Incorrect number of arguments!");
            ModPlayground.broadcastMessage("Example: /reset_system_name 0 0 0");
        }
        catch(NumberFormatException e){
            ModPlayground.broadcastMessage("[MOD][AAA] Error: Invalid arguments!");
            ModPlayground.broadcastMessage("Example: /reset_system_name 0 0 0");
        }
        catch(Exception e) {
            ModPlayground.broadcastMessage("[MOD][AAA] Error: Error removing name!");
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void serverAction(@Nullable PlayerState sender, String[] args) {

    }

    @Override
    public StarMod getMod() {
        return modInstance;
    }
}
